function luckySevens() {
	var firstroll = 0;
	var secondroll = 0;
	var sum = 0;
	var total = (document.getElementById('startingBet').value);
	total = Number(total.replace(/[^0-9\.]+/g,""))
	var play;
	var rolls = 0;
	var bet = total;
	var toptotal = 0;
	var toprolls = 0;
	while (total > 0) {
		if (total < 0) {
			document.getElementById("luckySevens").innerHTML += "<br>Must enter an amount greater than 0";
			break;
		}
		if (typeof total !== "number") {
			document.getElementById("luckySevens").innerHTML += "<br>Must enter a dollar amount or a number";
			break;
		}
		firstroll = Math.floor((Math.random() * 6) + 1);
		secondroll = Math.floor((Math.random() * 6) + 1);
		sum = firstroll + secondroll;
		if (sum == 7) {
			total += 4;
		}
		else {
			total -= 1;
		}
		rolls += 1;
		if (total > toptotal) {
			toptotal = total;
			toprolls = rolls;
		}
	}
	document.getElementById("displayResults").innerHTML = "<table id = \"results\"></table>"
	document.getElementById("displayResults").innerHTML = "<h3>Results</h3>" + document.getElementById("displayResults").innerHTML;
	var table = document.getElementById("results");
	var row = table.insertRow(0);
	var cell1 = row.insertCell(0);
	var cell2 = row.insertCell(1);
	var row = table.insertRow(1);
	var cell3 = row.insertCell(0);
	var cell4 = row.insertCell(1);
	var row = table.insertRow(2);
	var cell5 = row.insertCell(0);
	var cell6 = row.insertCell(1);
	var row = table.insertRow(3);
	var cell7 = row.insertCell(0);
	var cell8 = row.insertCell(1);
	cell1.innerHTML = "Starting Bet";
	cell2.innerHTML = bet;
	cell3.innerHTML = "Total Rolls Before Going Broke";
	cell4.innerHTML = rolls;
	cell5.innerHTML = "Highest Amount Won";
	cell6.innerHTML = toptotal;
	cell7.innerHTML = "Roll Count at Highest Amount Won";
	cell8.innerHTML = toprolls;
	document.getElementById("displayResults").innerHTML += "<br>"
	document.getElementById("btnPlayLuckySevens").innerHTML = "Play Again";
}